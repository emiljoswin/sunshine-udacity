package com.example.android.sunshine;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    ListView listView = null;
    ArrayList<String> weekForecast = new ArrayList<>();
    ArrayAdapter<String> adapter = null;
    public static final String EXTRA_MESSAGE = "com.example.android.sunshie.MainActivityFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //For handling menu items
        setHasOptionsMenu(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handling menu item clicks
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            updateWeather();
            return true;
        } else if (id == R.id.action_show_maps) {
            openPreferredLocationInMap();
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateWeather() {
        FetchWeatherTask weatherTask = new FetchWeatherTask();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String zipcode = prefs.getString(getString(R.string.pref_location_key),
                getString(R.string.pref_location_default));
        weatherTask.execute(zipcode);
    }

    public void openPreferredLocationInMap() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String zipcode = prefs.getString(getString(R.string.pref_location_key),
                getString(R.string.pref_location_default));

        //https://developer.android.com/guide/components/intents-common.html#Maps
        Uri geolocation = Uri.parse("geo:0,0?").buildUpon().appendQueryParameter("q", zipcode).build();

        Intent intent = new Intent(Intent.ACTION_VIEW, geolocation);
        if (getActivity().getPackageManager().getInstalledPackages(0) != null) {
            startActivity(intent);
        } else {
            Log.d("Error opening map", "Couldn't call " + zipcode + ", no receiving apps installed!");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        updateWeather();
    }
    private class FetchWeatherTask extends AsyncTask<String, Void, String> {

        private String getDay(long dateTime) {
            SimpleDateFormat shortenedDateFormat = new SimpleDateFormat("EEE MMM dd");
            String _day = shortenedDateFormat.format(dateTime);
            return _day;
        }


        @Override
        protected String doInBackground(String... params) {

            if (params.length == 0) {
                return null;
            }

            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String forecastJsonStr = null;

            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are avaiable at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast
                //URL url = new URL("http://api.openweathermap.org/data/2.5/forecast/daily?q=94043&mode=json&units=metric&cnt=7");

                final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";

                String format = "json";
                String units = "metric";
                int numDays = 7;


                final String QUERY_PARAM = "q";
                final String FORMAT_PARAM = "mode";
                final String UNITS_PARAM = "units";
                final String DAYS_PARAM = "cnt";

                Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                                    .appendQueryParameter(QUERY_PARAM, params[0])
                                    .appendQueryParameter(FORMAT_PARAM, format)
                                    .appendQueryParameter(UNITS_PARAM, units)
                                    .appendQueryParameter(DAYS_PARAM, Integer.toString(numDays))
                                    .build();

                URL url = new URL(builtUri.toString());
                Log.v("URL", url.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                forecastJsonStr = buffer.toString();

            } catch (IOException e) {
                Log.e("PlaceholderFragment", "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attemping
                // to parse it.
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }
            return forecastJsonStr;
        }

        @Override
        protected void onPostExecute(String forecastJsonStr) {
            //super.onPostExecute(s);

            JSONObject obj = null;

            if (forecastJsonStr == null) {
                Log.e("ERROR", "Didn't get response from server");
                return;
            }

            try {
                obj = new JSONObject(forecastJsonStr);
                JSONArray list = obj.getJSONArray("list");
                ArrayList<String> newWeekForecast = new ArrayList<>();

                Time dayTime = new Time();
                dayTime.setToNow();

                int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);
                long dateTime;
                String _day;
                boolean celsius = true;
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String listPrefs = prefs.getString(getString(R.string.listpref),
                        getString(R.string.list_default));

                if (listPrefs.equals("fahrenheit")) {
                    celsius = false;
                }

                for(int i = 0; i < 7; i++) {

                    dateTime = dayTime.setJulianDay(julianStartDay + i);
                    _day = getDay(dateTime);

                    JSONObject jsonObject = list.getJSONObject(i);
                    String main = jsonObject.getJSONArray("weather").getJSONObject(0).getString("main");
                    String maxTemp = jsonObject.getJSONObject("temp").getString("max");
                    String minTemp = jsonObject.getJSONObject("temp").getString("max");

                    if (!celsius) {
                        maxTemp = String.format("%.2f", Double.parseDouble(maxTemp) * 33.8);
                        minTemp = String.format("%.2f", Double.parseDouble(minTemp) * 33.8);
                    }

                    newWeekForecast.add(_day + " - " + main + ";  Temp - max:"+maxTemp + " min:" + minTemp);
                }

                adapter.clear();
                for(String s: newWeekForecast) {
                    adapter.add(s);
                }
            } catch (JSONException e) {
                Log.e("JSON_PARSE_ERROR", "json parse error");
                e.printStackTrace();
            }

        }
    }

    public MainActivityFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item_forecast,
                R.id.list_item_forecast_textview, new ArrayList<String>());
        adapter.clear();
        for(String s: weekForecast) {
            adapter.add(s);
        }
        adapter.notifyDataSetChanged();
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ListView) rootView.findViewById(R.id.listview_forecast);
        listView.setAdapter(adapter);


        // Adding a click listener for items in the list-view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = adapter.getItem(position);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra(EXTRA_MESSAGE, str);
                startActivity(intent);

            }
        });
        return rootView;
    }
}
